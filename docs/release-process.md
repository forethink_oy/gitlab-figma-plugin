# Release process

Publishing the plugin to the Figma plugin directory is a manual process. This project adopts a
Git-flow-inspired development workflow, where the [`master`](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/tree/master) branch is the source-of-truth.

## Prerequisites

You must have `admin` privileges of the [@gitlab](https://www.figma.com/@GitLab) Figma account to publish new versions of the plugin.

## Publish the plugin

When all features for a release have been merged to [`master`](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/tree/master) and you're ready to publish the plugin, action the following steps:

1. [Create a release](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/releases/new) in the [`gitlab-figma-plugin`](https://gitlab.com/gitlab-org/gitlab-figma-plugin) project with the appropriate version (following `semver`) and a brief changelog.
1. Generate a production-ready application bundle on your local machine for the `master` branch:

   ```bash
   # in the project's root directory
   git checkout master
   npm run build:clean
   ```

   This will ensure the `dist/` directory is up to date with the latest changes.

Now, within the Figma desktop application:

1. Sign in to the [@gitlab](https://www.figma.com/@GitLab) Figma account as an `admin` user. If you need help, please ask **@tomquirk**.
1. From the left sidebar, click **Plugins > Manage plugins**.
1. Find the **GitLab** plugin under **Development** on the right sidebar. If you cannot find this, ensure you have completed the [Figma setup](#figma-setup).
1. Check that the `manifest.json` file points to the correct file: check whether it is in the same directory as the `dist/` directory you wish to publish. You can do this by clicking the element to reveal the location of the `manifest.json` file.
1. Open the **Publish Plugin** page by hovering over the **GitLab** plugin to reveal a three-dots icon (ellipsis). Click the ellipsis and then **Publish New Release...**.
1. Add a brief changelog to the **Version notes** text area, as well as a link to the release in GitLab.
1. Publish the release by clicking **Publish**.

## Updating the plugin listing details

Updates to the plugin listing copy and assets (for example, cover art) are made within the Figma desktop
application in the **Publish Plugin** modal (see the final steps of the [Release process](#figma-plugin-releases)) section for a guide on publishing the plugin).

**Important**: we need to track the changes of marketing copy and assets for the plugin's listing in the
Figma plugin directory, and also ensure that this is reviewed by a GitLab Product Manager. When updating the plugin listing details:

1. Make any changes to the `figma_plugin_directory_assets/` directory.
1. Create a merge request, and request a review from a member of the ~"group::knowledge" ~frontend or product team.

Once its content is merged, follow the [Release process](#figma-plugin-releases) to make the approved changes to the plugin's listing on the **Publish Plugin** modal.
